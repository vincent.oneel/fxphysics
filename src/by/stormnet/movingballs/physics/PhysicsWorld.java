package by.stormnet.movingballs.physics;

import by.stormnet.movingballs.controller.PhysicsWorldCallckListener;
import by.stormnet.movingballs.shapes.AbstractShape;
import by.stormnet.movingballs.shapes.CircleShape;

import java.util.LinkedList;
import java.util.List;

public class PhysicsWorld extends Thread {
    private final double w;
    private final double h;
    private List<Body> bodies;
    private PhysicsWorldCallckListener listener;
    private boolean isRunning;
    private long delay;
    private long lastUpdateTime;

    public PhysicsWorld(String name, long delay, double width, double height) {
        super(name);
        this.delay = delay;
        w = width;
        h = height;
        bodies = new LinkedList<>();
    }

    public void registerCallbackListener(PhysicsWorldCallckListener listener){
        this.listener = listener;
    }

    @Override
    public void run() {
        isRunning = true;
        lastUpdateTime = System.currentTimeMillis();
        while (isRunning){
            bodies.forEach(b -> {
                if (b != null){
                    b.move(System.currentTimeMillis() - lastUpdateTime);
                    //TODO: implement border collisions
                    if (b.getShape() != null){
                        if (b.getShape() instanceof CircleShape) {
                            CircleShape sh = (CircleShape) b.getShape();
                            if (sh.getCenter().x + sh.getRadius() >= w
                                    || sh.getCenter().x - sh.getRadius() <= 0){
                                b.setvX(b.getvX() * -1);
                            }
                            if (sh.getCenter().y + sh.getRadius() >= h
                                    || sh.getCenter().y - sh.getRadius() <= 0){
                                b.setvY(b.getvY() * -1);
                            }
                        }
                    }
                }
            });
            lastUpdateTime = System.currentTimeMillis();
            if (listener != null){
                listener.worldCallback();
            }
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
                isRunning = false;
            }
        }
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }

    public Body createBody(){
        Body b = new Body();
        bodies.add(b);
        return b;
    }
}
