package by.stormnet.movingballs.physics;

import by.stormnet.movingballs.shapes.AbstractShape;
import by.stormnet.movingballs.shapes.Point;

public class Body {
    private double vX;
    private double vY;
    private AbstractShape shape;

    public Body(double vX, double vY, AbstractShape shape) {
        this.vX = vX;
        this.vY = vY;
        this.shape = shape;
    }

    public Body() {
        this.shape = shape;
    }

    public double getvX() {
        return vX;
    }

    public void setvX(double vX) {
        this.vX = vX;
    }

    public double getvY() {
        return vY;
    }

    public void setvY(double vY) {
        this.vY = vY;
    }

    public AbstractShape getShape() {
        return shape;
    }

    public void setShape(AbstractShape shape) {
        this.shape = shape;
    }

    public void move(long time){
        if (shape == null)
            return;
        shape.setCenter(new Point(shape.getCenter().x + vX * time, shape.getCenter().y + vY * time));
    }
}
