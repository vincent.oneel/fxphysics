package by.stormnet.movingballs.controller;

import by.stormnet.movingballs.gfx.Drawable;
import by.stormnet.movingballs.physics.Body;
import by.stormnet.movingballs.physics.PhysicsWorld;
import by.stormnet.movingballs.shapes.AbstractShape;
import by.stormnet.movingballs.shapes.CircleShape;
import by.stormnet.movingballs.shapes.Point;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

public class MainScreenController implements Initializable, PhysicsWorldCallckListener {

    @FXML
    private Canvas canvas;

    private List<Drawable> drawables;
    private PhysicsWorld world;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        drawables = new LinkedList<>();
        canvas.addEventHandler(MouseEvent.MOUSE_CLICKED, this::canvasClicked);
        world = new PhysicsWorld("Physics World", 10, canvas.getWidth(), canvas.getHeight());
        world.registerCallbackListener(this);
        world.start();
    }

    private void canvasClicked(MouseEvent evt){
        AbstractShape shape = new CircleShape(new Point(evt.getX(), evt.getY()),
                Color.CADETBLUE, 2.0, 50);
        drawables.add(shape);
        Body body = world.createBody();
        body.setShape(shape);
        body.setvX(1.0);
        body.setvY(0.5);

        //repaint();
    }

    private void repaint() {
        canvas.getGraphicsContext2D().clearRect(0, 0, 1280, 800);
        drawables.forEach((d) -> {
            if (d != null){
                d.draw(canvas.getGraphicsContext2D());
            }
        });
    }

    @Override
    public void worldCallback() {
        Platform.runLater(this::repaint);
    }
}
