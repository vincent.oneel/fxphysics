package by.stormnet.movingballs;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage wnd) throws Exception {
        wnd.setScene(new Scene(FXMLLoader.load(getClass().getResource("/views/MainScreenView.fxml")),
                1280,800));
        wnd.show();
    }
}
