package by.stormnet.movingballs.shapes;

import by.stormnet.movingballs.gfx.Drawable;
import javafx.scene.paint.Color;

public abstract class AbstractShape implements Drawable {
    protected Point center;
    protected Color color;
    protected double lineWidth;

    public AbstractShape(Point center, Color color, double lineWidth) {
        this.center = center;
        this.color = color;
        this.lineWidth = lineWidth;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public double getLineWidth() {
        return lineWidth;
    }

    public void setLineWidth(double lineWidth) {
        this.lineWidth = lineWidth;
    }
}