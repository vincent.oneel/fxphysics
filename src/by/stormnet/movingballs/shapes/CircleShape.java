package by.stormnet.movingballs.shapes;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class CircleShape extends AbstractShape {

    private double radius;

    private CircleShape(Point center, Color color, double lineWidth) {
        super(center, color, lineWidth);
    }

    public CircleShape(Point center, Color color, double lineWidth, double radius) {
        this(center, color, lineWidth);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setStroke(color);
        gc.setLineWidth(lineWidth);
        gc.strokeOval(center.x - radius, center.y - radius, radius * 2, radius * 2);
    }
}